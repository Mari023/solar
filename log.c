#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include "log.h"

_Noreturn static void die(const char message[]) {
    perror(message);
    exit(EXIT_FAILURE);
}

static void writeLine(FILE *file, struct tm *time, struct powerInfo *info) {
    char hours[] = "00";
    strftime(hours, sizeof(hours), "%H", time);
    char minutes[] = "00";
    strftime(minutes, sizeof(minutes), "%M", time);
    char seconds[] = "00";
    strftime(seconds, sizeof(seconds), "%S", time);
    int stringLength = snprintf(NULL, 0, "%f", info->dailyYield);
    if (stringLength < 0)
        die("snprintf");

    char dailyYield[stringLength];

    stringLength = sprintf(dailyYield, "%f", info->dailyYield);
    if (stringLength < 0)
        die("sprintf");
    char *dot = strchr(dailyYield, '.');
    if(dot[0] == '.')
        dot[0] = ',';
    stringLength = snprintf(NULL, 0, "%f", info->totalYield);
    if (stringLength < 0)
        die("snprintf");

    char totalYield[stringLength];

    stringLength = sprintf(totalYield, "%f", info->totalYield);
    if (stringLength < 0)
        die("sprintf");
    dot = strchr(totalYield, '.');
    if(dot[0] == '.')
        dot[0] = ',';
    fprintf(file, "%s:%s:%s;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;%s;%s;0;0;0;0;\n", hours, minutes, seconds,
            dailyYield, totalYield);
}

static void writeHeader(FILE *file, time_t epochTime, struct powerInfo *info) {
    struct tm time;
    if(localtime_r(&epochTime, &time) == NULL)
        die("localtime");
    char date[] = " 00.00.0000";
    strftime(date, sizeof(date), "%d.%m.%Y", &time);
    if (fprintf(file,
                "Datum: %s\nWR-Nr;Name;Typ;Serien-Nr;Strings;Leistung;\n1;UweWR;WR;90523LHD0000O;3;5800;\n\nEinheiten: U[V], I[mA], P[W], E[kWh], Ain [mV]\nZeit;WR-Nr;DC1 U;DC1 I;DC1 P;DC2 U;DC2 I;DC2 P;DC3 U;DC3 I;DC3 P;AC1 U;AC1 I;AC1 P;AC2 U;AC2 I;AC2 P;AC3 U;AC3 I;AC3 P;Ain1;Ain2;Ain3;Ain4;Tages E;Total E;Tages E Out;Total E Out;Tages E In;Total E In;\n",
                date) < 0) {
        die("fprintf");
    }

    epochTime -= 900;//subtract 15 minutes
    if(localtime_r(&epochTime, &time) == NULL)
        die("localtime");
    struct powerInfo info_copy;
    memcpy(&info_copy, info, sizeof(info_copy));
    info_copy.dailyYield = 0;
    info_copy.totalYield -= info->dailyYield;
    writeLine(file, &time, &info_copy);
}

float getLastYield(FILE *file) {
    return -1;//TODO
}

_Noreturn void logYield(const char basePath[], struct powerInfo *info) {
    time_t epochTime = time(NULL);
    if (epochTime == -1)
        die("time");

    struct tm *time = localtime(&epochTime);
    if (time == NULL)
        die("localtime");

    size_t length = 1 + 4 + 1 + 12 + 4 + 1;//slash, yearPath, slash, filename, file ending, null byte
    char datePath[length];
    strftime(datePath, sizeof(datePath), "/%Y/%Y%m%d.csv", time);
    char path[strlen(basePath) + length];
    path[0] = '\0';
    strcat(path, basePath);
    strcat(path, datePath);
    char yearPath[] = "/0000/";
    strftime(yearPath, sizeof(yearPath), "/%Y/", time);
    char folder[strlen(yearPath) + strlen(basePath) + 1];
    folder[0] = '\0';
    strcat(folder, basePath);
    strcat(folder, yearPath);

    if (mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1 && errno != EEXIST)
        die("mkdir");

    bool fileDoesNotExist = false;
    struct stat file_stat;
    if (stat(path, &file_stat) == -1) {
        if (errno == ENOENT) {
            fileDoesNotExist = true;
        } else {
            die("stat");
        }
    }

    FILE *file = fopen(path, "a+");
    if (file == NULL)
        die("fopen");
    if (fileDoesNotExist) {
        writeHeader(file, epochTime, info);
    } else {
        if (getLastYield(file) == info->dailyYield) {
            fclose(file);
            exit(EXIT_SUCCESS);
        }
    }

    writeLine(file, time, info);

    fclose(file);
    exit(EXIT_SUCCESS);
}
