#include <stdlib.h>
#include <modbus/modbus.h>
#include <stdio.h>
#include <errno.h>
#include "log.h"
#include "powerinfo.h"

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "usage: %s <path> <ip> <port>\n", argv[0]);
    }
    const char *path = argv[1];
    const char *ip = argv[2];
    const char *port = argv[3];

    modbus_t *ctx = modbus_new_tcp_pi(ip, port);
    if (ctx == NULL) {
        fprintf(stderr, "Unable to allocate libmodbus context\n");
        exit(EXIT_FAILURE);
    }
    if (modbus_connect(ctx) == -1) {
        fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        exit(EXIT_FAILURE);
    }
    modbus_set_slave(ctx, 71);

    u_int16_t result[2];
    struct powerInfo info;
    if (modbus_read_registers(ctx, 322, 2, result) == -1) {
        printf("modbus_read_registers: %s\n", modbus_strerror(errno));

        modbus_close(ctx);
        modbus_free(ctx);
        exit(EXIT_FAILURE);
    }
    info.dailyYield = modbus_get_float_cdab(result) / 1000;
    if (modbus_read_registers(ctx, 320, 2, result) == -1) {
        printf("modbus_read_registers: %s\n", modbus_strerror(errno));

        modbus_close(ctx);
        modbus_free(ctx);
        exit(EXIT_FAILURE);
    }
    info.totalYield = modbus_get_float_cdab(result) / 1000;

    modbus_close(ctx);
    modbus_free(ctx);

    if (info.dailyYield == 0)
        exit(EXIT_SUCCESS);
    logYield(path, &info);
}
