.PHONY: all, clean, test, valgrind

CC = gcc
CFLAGS = -std=c11 -pedantic -Wall -Werror -D_XOPEN_SOURCE=700
LIBMODBUS = /usr/lib/x86_64-linux-gnu/libmodbus.so

all: solar

clean:
	rm -f solar.o solar

%.o: %.c
	$(CC) $(CFLAGS) -c $<

log.o: log.c log.h powerinfo.h

solar.o: solar.c log.h powerinfo.h

solar: solar.o log.o log.h powerinfo.h
	$(CC) $(CFLAGS) -o $@ $^ $(LIBMODBUS)

test: solar
	./solar . 192.168.188.20 1502

valgrind: solar
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./solar . 192.168.188.20 1502