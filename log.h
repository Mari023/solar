#ifndef LOG
#define LOG

#include "powerinfo.h"

_Noreturn void logYield(const char basePath[], struct powerInfo *info);

#endif
