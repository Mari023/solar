#ifndef POWERINFO
#define POWERINFO

struct powerInfo {
    float dailyYield;
    float totalYield;
};

#endif
